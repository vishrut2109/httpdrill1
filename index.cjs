const http = require("http");
const { v4: uuidv4 } = require("uuid");
const fs = require("fs");

const host = "localhost";
const port = process.env.PORT || 8000;

const requestListener = function (request, response) {
  const requestURL = request.url;
  const status = Number(requestURL.split("/").slice(-1));
  const delayBySeconds = Number(requestURL.split("/").slice(-1));

  if (requestURL === "/html" && request.method === "GET") {
    // returns htmlContent
    fs.readFile("htmlContent.html", "utf8", (err, htmlData) => {
      if (err) {
        console.error(err);
        response.writeHead(500, { "Content-Type": "text/html" });
        response.write("Error in reading file htmlContent");
      } else {
        response.writeHead(200, { "Content-Type": "text/html" });
        response.write(htmlData);
      }
      response.end();
    });
  } else if (requestURL == "/json" && request.method === "GET") {
    // returns jsonContent
    fs.readFile("jsonContent.json", "utf8", (err, jsonData) => {
      if (err) {
        console.error(err);
        response.writeHead(500, { "Content-Type": "application/json" });
        response.write("Error in reading jsonContent");
      } else {
        console.log(jsonData);
        response.writeHead(200, { "Content-Type": "application/json" });
        response.write(jsonData);
      }
      response.end();
    });
  } else if (requestURL === "/uuid" && request.method === "GET") {
    // returns a different UUID4 on each request.
    const id = uuidv4();
    const uuidData = {
      uuid: id,
    };
    response.writeHead(200, { "Content-Type": "application/json" });
    response.write(JSON.stringify(uuidData));
    response.end();
  } else if (requestURL === `/status/${status}` && request.method === "GET") {
    //  return a response with a status code as specified in the request.
    const httpStatusCode = http.STATUS_CODES[status];

    if (httpStatusCode === undefined) {
      const statusCode = 400;
      response.writeHead(statusCode, { "Content-Type": "application/json" });

      const errorMessage = {
        message: http.STATUS_CODES[statusCode],
        error: "Status code is invalid",
      };

      response.write(JSON.stringify(errorMessage));
      response.end();
    } else {
      response.writeHead(status, { "Content-Type": "application/json" });
      response.write(`Status Code Response: ${httpStatusCode}`);
      response.end();
    }
  } else if (
    requestURL === `/delay/${delayBySeconds}` &&
    request.method === "GET"
  ) {
    // delaying response for given number of seconds
    if (delayBySeconds >= 0) {
      setTimeout(() => {
        response.writeHead(200, { "Content-Type": "text/plain" });
        response.write(`Response delayed for ${delayBySeconds} seconds`);
        response.end();
      }, delayBySeconds * 1000);
    } else {
      const statusCode = 400;
      response.writeHead(statusCode, { "Content-Type": "application/json" });

      const errorMessage = {
        message: http.STATUS_CODES[statusCode],
        error: "Cannot delay time for negative values",
      };

      response.write(JSON.stringify(errorMessage));
      response.end();
    }
  } else {
    response.writeHead(500, { "Content-Type": "application/json" });

    const errorMessage = {
      message: "Internal Server Error",
      error: "Invalid request",
    };
    response.write(JSON.stringify(errorMessage));
    response.end();
  }
};

const server = http.createServer(requestListener);
server.listen(port, host, () => {
  console.log(`Server is running on http://${host}:${port}`);
});
